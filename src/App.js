import Movies from "./components/Movies"
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router, Route} from "react-router-dom";
import Signing from "./components/Signing";

function App() {
    return (
        <Router>
            <div className="App">
                <Route path="/" exact component={Movies} />
                <Route path="/signing" component={Signing} />
            </div>
        </Router>
    );
}

export default App;
