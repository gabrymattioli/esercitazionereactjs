import React from 'react';
import axios from 'axios'
import Button from 'react-bootstrap/Button'

class AddMovie extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            subtitle: '',
            director: '',
            description: '',
        }
    }

    async addFilm(event) {
        event.preventDefault();

        await axios.post("http://127.0.0.1:8000/api/movies/ ", this.state).catch(err => { console.log(err) });
        this.props.get();

        this.setState({
            title: '',
            subtitle: '',
            director: '',
            description: '',
        })

        this.props.onAdd();
    }

    render() {
        return (
            <form onSubmit={(e) => this.addFilm(e)} >
                <div className="form-row d-flex align-items-center justify-content-around">
                    <div>
                        <label>Titolo</label>
                        <input className="form-control" type="text" placeholder="Titolo" value={this.state.title}
                               onChange={(e) => this.setState({ title: e.target.value })} />
                    </div>
                    <div>
                        <label>Sottotitolo</label>
                        <input className="form-control" type="text" placeholder="Sottotitolo" value={this.state.subtitle}
                               onChange={(e) => this.setState({ subtitle: e.target.value })}/>
                    </div>
                </div>
                <div className="form-row d-flex align-content-center justify-content-around mt-3">
                    <div>
                        <label>Regista</label>
                        <input className="form-control" type="text" placeholder="Regista" value={this.state.director}
                               onChange={(e) => this.setState({ director: e.target.value })}/>
                    </div>
                    <div>
                        <label>Descrizione</label>
                        <input className="form-control" type="text" placeholder="Descrizione" value={this.state.description}
                               onChange={(e) => this.setState({ description: e.target.value })}/>
                    </div>
                </div>
                <div className="d-flex align-items-center justify-content-end">
                    <Button className="mt-3" variant="danger" type="submit">Salva</Button>
                </div>
            </form>
        )
    }
}

export default AddMovie