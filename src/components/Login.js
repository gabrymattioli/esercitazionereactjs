import React from 'react';
import Button from 'react-bootstrap/Button'
import axios from "axios";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        }
    }

    async login(event) {
        event.preventDefault();

        console.log(localStorage.getItem('token'))
        axios.post("http://localhost:8000/api-auth/login/ ", this.state).then(
            res => localStorage.setItem('token', res.token)
        ).catch(err => { console.log(err) });


        this.setState({
            username: '',
            password: '',
        })
    }

    render() {
        return (
            <div className="login">
                <form onSubmit={(e) => this.login(e)}>
                    <div className="form-row d-flex align-items-center justify-content-around">
                        <div>
                            <label>Username</label>
                            <input className="form-control" type="text" placeholder="Username" value={this.state.username}
                                   onChange={(e) =>
                                       this.setState({ username: e.target.value })} />
                        </div>
                    </div>
                    <div className="form-row d-flex align-items-center justify-content-around mt-3">
                        <div>
                            <label>Password</label>
                            <input className="form-control" type="password" placeholder="Password" value={this.state.password}
                                   onChange={(e) =>
                                       this.setState({ password: e.target.value })} />
                        </div>
                    </div>
                    <div className="d-flex align-items-center justify-content-end">
                        <Button className="mt-3" variant="danger" type="submit">Login</Button>
                    </div>
                </form>
            </div>
        )
    }
}

export default Login