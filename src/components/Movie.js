import { FaTrash } from "react-icons/fa";
import React from "react";
import PropTypes from 'prop-types';
import axios from "axios";

const Movie = (props) => {
    const deleteMovie = async(id) => {
        await axios.delete("http://127.0.0.1:8000/api/movies/" + id).catch(err => { console.log(err) });
        props.get();
    }

    return (
        <div className="movie" onDoubleClick={() => alert('ciaoooo')}>
            <div className="movie-metadata">
                <h6 className="title">{props.movie.title}</h6>
                <p className="subtitle">{props.movie.subtitle}</p>
            </div>
            <div className="movie-action">
                <FaTrash style={{color: 'red', fontSize: '120%'}} onClick={() => deleteMovie(props.movie.id)}/>
            </div>
        </div>
    )
}

Movie.propTypes = {
    movie: PropTypes.object,
    get: PropTypes.func,
}

export default Movie