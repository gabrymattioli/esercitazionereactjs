import axios from 'axios'
import React from 'react'
import Movie from "./Movie";
import AddMovie from "./AddMovie";
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';

class Movies extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            movies: [],
            addMovie: false,
            newText: 'Nuovo +'
        }
    }

    componentDidMount() {
        this.getMovies();
    }

    getMovies() {
        axios.get("http://127.0.0.1:8000/api/movies").then(
            res => {this.setState({ movies: res.data });
            }).catch(err => { console.log(err) });
    }

    toggleNew() {
        this.setState({
            addMovie: (!this.state.addMovie),
            newText: this.state.newText === 'Nuovo +' ? 'Annulla' : 'Nuovo +',
        });
    }

    render() {
        return (
            <div className="movies box">
                <div className="movie-header">
                    <h4>I tuoi film preferiti</h4>
                    <div>
                        <Button variant="info" onClick={this.toggleNew.bind(this)}>
                            {this.state.newText}
                        </Button>
                        <Link to="/signing">
                            <Button className="ml-2" variant="dark" >Login</Button>
                        </Link>
                    </div>
                </div>
                <hr />
                { this.state.addMovie ?
                    <AddMovie get={this.getMovies.bind(this)} onAdd={this.toggleNew.bind(this)} />
                    :
                    <>
                        { this.state.movies.map(movie => (
                            <Movie key={movie.id} movie={movie} get={this.getMovies.bind(this)} />
                        ))}
                    </>
                }
            </div>
        )
    }
}

export default Movies