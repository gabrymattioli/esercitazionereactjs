import React from 'react';
import Login from './Login';
import Button from "react-bootstrap/Button";
import {Link} from "react-router-dom";

class Signing extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: true,
            title: 'Login',
            buttonText: 'Nuovo account',
        }
    }

    render() {
        return (
            <div className="signing box">
                <div className="signing-header">
                    <h4>{this.state.title}</h4>
                    <div>
                        <Button variant="info">
                            {this.state.buttonText}
                        </Button>
                        <Link to="/">
                            <Button className="ml-2" variant="dark" >Annulla</Button>
                        </Link>
                    </div>
                </div>
                <hr />
                <Login />
            </div>
        )
    }
}

export default Signing
